#ifndef AVERAGE
#define AVERAGE

#include <cmath>
#include <limits>

class Average
{
public:
  Average(){sum = squareSum = count = 0;
    amax = std::numeric_limits<double>::min();
    amin = std::numeric_limits<double>::max();}
  Average(double s, double ss, int c, double x, double n)
    {sum = s; squareSum = ss; count = c; amax = x; amin = n;}

  ~Average(){}

  double getAvg(){ if(count > 0) return sum/(double)count; return 0;}
  int getCount(){ return count; }
  double getMax(){ return amax; }
  double getMin(){ return amin; }
  double getSum(){ return sum; }
  double getSumSquares(){ return squareSum; }
  double getStdDiv(){
    if(count > 1) return sqrt((squareSum - sum*sum / count)/(count-1));
    return 0;}

  void add(double n);

  Average operator+(Average& right);

private:
  double sum;
  double squareSum; // sum of squares
  int count;
  double amax;
  double amin;

};

void Average::add(double n)
{
  if(n > amax || amax == std::numeric_limits<double>::min())
    amax = n;
  if(n < amin || amin == std::numeric_limits<double>::max())
    amin = n;

  sum += n;
  squareSum += n*n;
  count++;
}

Average Average::operator+(Average &right) 
{
  Average result;
  double s = sum + right.getSum();
  double ss = squareSum + right.getSumSquares();
  double min = amin, max = amax;
  int cnt = count + right.getCount();

  if(right.amin < min)
    min = right.amin;
  
  if(right.amax > max)
    max = right.amax;

  return Average(s, ss, cnt, max, min);
}

#endif
