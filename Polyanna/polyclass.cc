#include "PolyClass.hh"

Frizell::Frizell(string n, int nu, int h, bool p, int l)
{
  participating=p;
  name = n;
  number = nu;
  household = h;
  lastPoly=nu;
  lastPoly = l;
  if(!participating)
    polyanna = make_pair(number, household);
  // polyanna is initialized in shuffle
}

Frizell::~Frizell()
{
  // doesn't explicitly need anything
}

// if the household of this person is the household of the
// polyanna (polyanna.second) OR if the last polyanna that 
// this person had was this same person, then
// this person can not have this polyanna.
bool Frizell::canHavePolyanna()
{
  if(household == polyanna.second 
     || lastPoly == polyanna.first)
    return false;
  return true;
}

PolyannaGroup::PolyannaGroup()
{
  // doesn't explicitly need anything
}

PolyannaGroup::~PolyannaGroup()
{
  // doesn't explicitly need anything
}

void PolyannaGroup::shuffle()
{
  MTRand r;
  
  do{

    for(int i = 0; i < peopleInIt.size(); i++)
      peopleInIt[i].polyanna = make_pair(peopleInIt[i].number, peopleInIt[i].household);

    for(int i = peopleInIt.size()-1; i > 0; i--)
      swap(peopleInIt[i].polyanna, peopleInIt[r.randInt(i-1)].polyanna);
    
  }while(!isGoodShuffle());

  return;
}

void PolyannaGroup::add(string name, int num, int house, bool participating, int last)
{
  if(participating)
    peopleInIt.push_back(Frizell(name, num, house, participating, last));
  else
    notParticipating.push_back(Frizell(name, num, house, participating, last));
  return;
}

void PolyannaGroup::print(ostream &out)
{
  for(int i = 0; i < peopleInIt.size(); i++)
    out << setw(23) << peopleInIt[i].name << ','
	<< setw(6) << peopleInIt[i].number << ','
	<< setw(5) << thisgroupID+1 << ','
	<< setw(5) << peopleInIt[i].household << ','
	<< setw(5) << peopleInIt[i].participating << ','
      	<< setw(6) << peopleInIt[i].polyanna.first << '\n';
    
  for(int i = 0; i < notParticipating.size(); i++)
    out << setw(23) << notParticipating[i].name << ','
	<< setw(6) << notParticipating[i].number << ','
	<< setw(5) << thisgroupID+1 << ','
	<< setw(5) << notParticipating[i].household << ','
	<< setw(5) << notParticipating[i].participating << ','
      	<< setw(6) << notParticipating[i].polyanna.first << '\n';
    
  return;
}

bool PolyannaGroup::isGoodShuffle()
{
  bool isGood = true;
  for(int i = 0; i < peopleInIt.size(); i++)
    isGood *= peopleInIt[i].canHavePolyanna();
  return isGood;
}

// Void PolyannaGroup::swap(Frizell *&l, Frizell *&r)
// {
//   Frizell *s = l;
//   l = r;
//   r = s;
//   return;
// }
