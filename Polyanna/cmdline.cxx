#include "cmdline.h"

CmdLine::CmdLine()
{
  exit(EXIT_FAILURE);
}

CmdLine::CmdLine(int argc, char* argv[])
{
  for(int i = 1; i <= argc; i++)
    if(argv[i])
      input.push_back(string(argv[i]));

}

CmdLine::~CmdLine()
{

}

CmdLine::CmdLineArg::CmdLineArg(string na, bool req, int num)
{
  name = na;
  nargs = num;
  required = req;
  accessed = -1;
}

CmdLine::CmdLineArg::~CmdLineArg()
{
}


void CmdLine::addArgument(string arg, char argswitch,
			  bool required, int nargs)
{

  args.push_back(CmdLineArg(arg, required, nargs));

  CmdLineArg* argptr = find(arg);
  bool argFound = false;
  bool startFound = false;
  bool endFound = false;
  int current = 0;
  int count = 0;
  vector<string>::iterator startit = input.begin();
  vector<string>::iterator endit = input.begin();
  
  // find the start, plant the iterator
  if(argswitch != '-')
    while(startit != input.end() && !startFound)
      {
	if(input[current][0] == '-' && input[current][1] == argswitch)
	  startFound = true;
	else
	  startit++;
	// --
	current++;
	endit++;
      }
  else
    {
      if(args.size() > 1)
	{
	  // when using the default argument, add it (in code)
	  // BEFORE ANYthing else!
	  std::cerr << "Arguments added incorrectly:"
		    << " talk to the coder." << std::endl;
	  exit(EXIT_FAILURE);
	}
      else if(input.size() > 0)
	{
	  if(input[current][0] == '-' && required)
	    reportError(BAD_DEFAULT);
	  startFound = true;
	}
    }

  // find the end, storing the parameters
  if(startFound)
    while(endit != input.end() && !endFound)
      {
	// in the first case, we don't need the current
	// argument, however, in the second we do.
	if(input[current][0] == '-')
	  endFound = true;
	else
	  {
	    if(current == (input.size() - 1))
	      endFound = true;
	    argptr->args.push_back(input[current]);
	    count++;
	    endit++;
	    current++;
	  }
      }
  
  input.erase(startit, endit);
  
 
  if(startFound && endFound)
    argFound = true;
  
  if(required && !argFound)
    reportError(REQUIRED_NOT_FOUND);

  if(argptr->nargs && argptr->args.size())
    {
      if(argptr->args.size() < nargs)
	reportError(TOO_FEW_ARGS);
      if(argptr->args.size() > nargs)
	reportError(TOO_MANY_ARGS);
    }

  return;
}


string CmdLine::getArgument(string arg)
{
  int n;
  CmdLineArg* argptr = find(arg);
  ++(argptr->accessed);
  if(argptr->accessed < argptr->args.size())
    return argptr->args[(argptr->accessed)];
  else
    return argptr->args[argptr->accessed = 0];
}

int CmdLine::getNumArgs(string arg)
{
  return find(arg)->args.size();
}

void CmdLine::reportError(CmdError err)
{
  if(err == REQUIRED_NOT_FOUND)
    std::cerr << "Required argument not found." << std::endl;
  if(err == TOO_MANY_ARGS)
    std::cerr << "Too many arguments found for switch." << std::endl;
  if(err == TOO_FEW_ARGS)
    std::cerr << "Too few arguments found for switch." << std::endl;
  if(err == BAD_DEFAULT)
    std::cerr << "Default argument is required and should be first." << std::endl;

  exit(EXIT_FAILURE);
}

CmdLine::CmdLineArg* CmdLine::find(string arg)
{
  CmdLineArg* argument = NULL;
  for(int i = 0; i < args.size(); i++)
    if(args[i].name == arg)
      argument = &args[i];
  return argument;
}
