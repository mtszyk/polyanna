// make a list of people
// make a list of households
// re-read list, output with "name" has "polyanna" of "household"

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include "CmdLine.h"

using std::ifstream;
using std::ofstream;
using std::map;
using std::endl;

void getPolyannaList(ifstream &in, map<int,string> &people);
void getHouseholdList(ifstream &in, map<int,string> &households);
void printFiles(map<int,string> &people,
		map<int,string> &households,
		ifstream &in,
		string outdir);

int main(int argc, char* argv[])
{
  CmdLine cmd(argc, argv);
  cmd.addArgument("args", '-', true, 3);
  // 3 input parameters: people list, household list, output DIRECTORY.
  // like PrintPolyanna 2008.polyanna households.dat ./output/

  string polyannaIn = cmd.getArgument("args");
  string householdsIn = cmd.getArgument("args");
  string outdir = cmd.getArgument("args");

  map<int,string> people;
  map<int,string> households;

  ifstream infile(polyannaIn.c_str());
  if(!infile.good())
    {
      std::cout << "Check polyanna filename." << endl;
      exit(1);
    }

  getPolyannaList(infile, people);

  infile.close();
  infile.clear();

  infile.open(householdsIn.c_str());
  if(!infile.good())
    {
      std::cout << "Check household filename." << endl;
      exit(2);
    }

  getHouseholdList(infile, households);

  infile.close();
  infile.clear();

  infile.open(polyannaIn.c_str());

  printFiles(people, households, infile, outdir);

  infile.close();
  infile.clear();

  return 0;
}


 void getPolyannaList(ifstream &in, map<int,string> &people)
 {
   string line;

   while(getline(in, line))
     {
       int pos = line.find(','); // "comma delimited"

       // get name
       string name = line;
       name.erase(pos); // erase everything after the name
       name.erase(0, name.find_first_not_of(' ')); // erase everything before it

      // get numbers
      // ++cptr means look at the next character
      // after the delimiting character, basically.
      // ANY character can be used to delimit the numbers,
      // from a single space to a comma followed by 20 spaces,
      // but there may only be ONE delimiting character,
      // followed by whitespace!
      char *cptr = (char*)line.c_str()+pos;
      int myID = (int)strtod(++cptr,&cptr);
      int groupID = (int)strtod(++cptr,&cptr);
      int houseID = (int)strtod(++cptr,&cptr);
      bool participating = (bool)(int)strtod(++cptr,&cptr);
      int lastPoly = (int)strtod(++cptr,&cptr);

      people[myID] = name;
     }

     return;
 }

void getHouseholdList(ifstream &in, map<int,string> &households)
{
  string line;

  while(getline(in, line))
    {
      int pos = line.find(','); // "comma delimited"

      // get name
      string name = line;
      name.erase(pos); // erase everything after the name
      name.erase(0, name.find_first_not_of(' ')); // erase everything before it

      // get numbers
      // ++cptr means look at the next character
      // after the delimiting character, basically.
      // ANY character can be used to delimit the numbers,
      // from a single space to a comma followed by 20 spaces,
      // but there may only be ONE delimiting character,
      // followed by whitespace!
      char *cptr = (char*)line.c_str()+pos;
      int myID = (int)strtod(++cptr,&cptr);

      households[myID] = name;
    }

  return;
}

void printFiles(map<int,string> &people,
		map<int,string> &households,
		ifstream &in,
 		string outdir)
{
  string line;
  int i=0;

  outdir = outdir+"/";

  people.size();

  while(getline(in, line))
    {
      int pos = line.find(','); // "comma delimited"

      // get name
      string name = line;
      name.erase(pos); // erase everything after the name
      name.erase(0, name.find_first_not_of(' ')); // erase everything before it

      // get numbers
      // ++cptr means look at the next character
      // after the delimiting character, basically.
      // ANY character can be used to delimit the numbers,
      // from a single space to a comma followed by 20 spaces,
      // but there may only be ONE delimiting character,
      // followed by whitespace!
      char *cptr = (char*)line.c_str()+pos;
      int myID = (int)strtod(++cptr,&cptr);
      int groupID = (int)strtod(++cptr,&cptr);
      int houseID = (int)strtod(++cptr,&cptr);
      bool participating = (bool)(int)strtod(++cptr,&cptr);
      int polyanna = (int)strtod(++cptr,&cptr);


      if(participating)
	{
	  string outfilename = outdir + name + ".txt";

	  std::cout << outfilename << endl;

	  ofstream outfile(outfilename.c_str());

	  if(!outfile.good())
	    {
	      std::cout << "Error opening file:   " << name << ".txt"<< endl;
	      exit(2);
	    }
	  outfile << "Hi " << name << ", this year your pollyanna is " << people[polyanna] << "!\n"
		  << "Suggested Expenditures (where *** is your group):\n";

	  switch(groupID)
	    {
	    case 1:
	      outfile << "    The Little Ones : $15 - $20\n"
		      << "    The Cousins : $25 - $30\n"
		      << "*** The Siblings : $50 - $60\n\n";
	      break;
	    case 2:
	      outfile << "    The Little Ones : $15 - $20\n"
		      << "*** The Cousins : $25 - $30\n"
		      << "    The Siblings : $50 - $60\n\n";
	      break;
	    case 3:
	      outfile << "*** The Little Ones : $15 - $20\n"
		      << "    The Cousins : $25 - $30\n"
		      << "    The Siblings : $50 - $60\n\n";
	      break;
	    }

	  outfile << "Don't forget to update your wishlist!";

      /*
      outfile << "Please fill out your wish list on the family web site! An online\n"
              << "invite and sign up sheet will be following for food and supplies.\n\n"
              << "At least, they were last year. I'm just running the program over here. They don't tell me anything.\n\n" ;


	  outfile << "Details for this year's event:\n"
		  << "Date and Time: December 31, 2:30 pm - 7:30 pm\n"
		  << "Location: The National Park Fire Hall\n"
		  << "          523 Hessian Ave \n"
		  << "          National Park, NJ 08063 \n\n";
          */
      outfile << "DETAILS TO FOLLOW\n";

	  outfile << "Individual invitations are NOT inspected, so if there \n"
		  << "are any problems with the info you received, please \n"
		  << "e-mail Matt Simmons at matt.simmons@compbiol.org IMMEDIATELY!\n\n"
		  << "Merry Christmas, and happy Pollyanna!\n";
	  outfile.close();
	}

      ++i;
    }

  return;
}

