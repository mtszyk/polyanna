#ifndef POLYCLASSES_M
#define POLYCLASSES_M
#define NUMGROUPS 3

#include <utility> // pair
#include <vector>
#include <string>
#include <ostream>
#include <iomanip>

#include "MersenneTwister.h"

using std::swap;
using std::pair;
using std::make_pair;
using std::setw;
using std::string;
using std::vector;
using std::ostream;

// each person
class Frizell
{
public:
  Frizell(string n, int nu, int h, bool p, int l);
  ~Frizell();

  string name;
  int lastPoly;
  int number;
  int household;
  int group;
  bool participating;
  pair<int, int> polyanna; // first=number, second=household of polyanna
  bool canHavePolyanna();
};

// the list
class PolyannaGroup
{
public:
  PolyannaGroup();
  ~PolyannaGroup();

  void shuffle();
  void add(string name, int num, int house, bool participating, int last);
  void print(ostream &out);

  void setGroup(int g) {thisgroupID=g;}

private:
  vector<Frizell> peopleInIt;
  vector<Frizell> notParticipating;
  int thisgroupID;
  bool isGoodShuffle();

};

#endif
