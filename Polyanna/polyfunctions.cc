#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

#include "PolyClass.hh"

using namespace std;
using std::string;
using std::ifstream;

PolyannaGroup *getData(ifstream &in, PolyannaGroup *list)
{
  string line;

  while(getline(in, line))
    {
      int pos = line.find(','); // "comma delimited"

      // get name
      string name = line;
      name.erase(pos); // erase everything after the name
      name.erase(0, name.find_first_not_of(' ')); // erase everything before it

      // get numbers
      // ++cptr means look at the next character
      // after the delimiting character, basically.
      // ANY character can be used to delimit the numbers,
      // from a single space to a comma followed by 20 spaces,
      // but there may only be ONE delimiting character,
      // followed by whitespace!
      char *cptr = (char*)line.c_str()+pos;
      int myID = (int)strtod(++cptr,&cptr);
      int groupID = (int)strtod(++cptr,&cptr);
      int houseID = (int)strtod(++cptr,&cptr);
      bool participating = (bool)(int)strtod(++cptr,&cptr);
      int lastPoly = (int)strtod(++cptr,&cptr);

      /*
      cout << name << endl <<
	      myID << endl <<
	      houseID << endl <<
	      participating << endl <<
	      lastPoly << endl << 
	      groupID << endl << endl; */
      list[(groupID-1)].add(name, myID, houseID, participating, lastPoly);
    }

  return list;
}
