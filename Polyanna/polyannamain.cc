#include <fstream>
#include <iostream>
#include <string>
#include "PolyFunctions.hh"
#include "PolyClass.hh"
#include "CmdLine.h"

using std::ifstream;
using std::ofstream;

int main(int argc, char* argv[])
{
  CmdLine cmd(argc, argv);
  cmd.addArgument("args", '-', true, 2); //input and output
  // like PolyannaShuffle in.dat out.dat

  string infilename = cmd.getArgument("args");
  string outfilename = cmd.getArgument("args");

  if(outfilename == infilename)
    {
      std::cout << "Input and output can not be identical." << std::endl;
      exit(1);
    }

  // processing
  ifstream infile(infilename.c_str());
  ofstream outfile(outfilename.c_str());

  if(!infile.good() || !outfile.good())
    {
      std::cout << "Check file names." << std::endl;
      exit(2);
    }

  PolyannaGroup *poly = new PolyannaGroup[3];
  for(int i = 0; i < NUMGROUPS; i++)
    poly[i].setGroup(i);

  getData(infile, poly);

  for(int i = 0; i < NUMGROUPS; i++)
    poly[i].shuffle();

  // output
  for(int i = 0; i < NUMGROUPS; i++)
    poly[i].print(outfile);

  // cleanup
  infile.close();
  outfile.close();
  //  delete poly; // why not? this causes major errors.

  return 0;
}
