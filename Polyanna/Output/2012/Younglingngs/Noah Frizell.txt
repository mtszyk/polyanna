Hey Noah Frizell, this year your pollyanna is Colin Frizell!
Suggested Expenditures (where *** is your group):
*** The Little Ones : $15 - $20
    The Cousins : $25 - $30
    The Siblings : $50 - $60

Please fill out your wish list on the family web site! An online
invite and sign up sheet will be following for food and supplies.

DETAILS TO FOLLOW
Individual invitations are NOT inspected, so if there 
are any problems with the info you recieved, please 
e-mail or call Matt Simmons at msimmons@udel.edu or 
856-803-0030 IMMEDIATELY (e-mail pref)!

Merry Christmas, and happy Pollyanna!
