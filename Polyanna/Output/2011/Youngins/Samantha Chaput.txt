It's pollyanna time again 
It falls on New Years Eve 
December 31st is the date 
Its at 2:30 don't be late! 
The party will be at the Fire Station 
This is a new location 
By 7:30 we must be out! 
For another party to come about. 

Hey Samantha Chaput, this year your pollyanna is Jonah Hilbert!
Suggested Expenditures (where *** is your group):
*** The Little Ones : $15 - $20
    The Cousins : $25 - $30
    The Siblings : $50 - $60

Please fill out your wish list on the family web site! An online
invite and sign up sheet will be following for food and supplies.

Details for this year's event:
Date and Time: December 31, 2:30 pm - 7:30 pm
Location: The National Park Fire Hall
          523 Hessian Ave 
          National Park, NJ 08063 

Individual invitations are NOT inspected, so if there 
are any problems with the info you recieved, please 
e-mail or call Matt Simmons at msimmons@udel.edu or 
856-803-0030 IMMEDIATELY (e-mail pref)!

Merry Christmas, and happy Pollyanna!
