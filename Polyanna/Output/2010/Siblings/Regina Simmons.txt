Hey Regina Simmons, this year your pollyanna is Mike Cucuglielo!
Suggested Expenditures (where *** is your group):
    The Little Ones : $15 - $20
    The Cousins : $25 - $30
*** The Siblings : $50 - $60

Details for this year's event:
Date and Time: Saturday, Jan 1. 3:00-8:00
Location: The AOH (Old Knights of Columbus) 
          200 Columbia Blvd 
          National Park, NJ 08063 
The cost will be 6 dollars per person.
Please contact Amy McCall or Regina Simmons for food.

Individual invitations are NOT inspected, so if there 
are any problems with the info you recieved, please 
e-mail or call Matt Simmons at msimmons@udel.edu or 
856-803-0030 IMMEDIATELY (e-mail pref)!

Merry Christmas, and happy Pollyanna!
