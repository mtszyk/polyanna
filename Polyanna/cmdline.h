// **********************************************
// Command line parsing argument parsing class
// Author: Matthew Simmons, 2008
// **********************************************


#ifndef COMMANDLINEARGPARSER_H
#define COMMANDLINEARGPARSER_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>

using std::vector;
using std::string;


// ******************************************************
// *** TODO:
//       - add true/false switches
//
// ******************************************************


// ******************************************************
// simple class that organizes command arguments
// in an easy to access manner.
// ------------------------
// add types of arguments with the add function,
// then scroll through the using the get function.
// ******************************************************
class CmdLine
{
 public:

  CmdLine(); // exits, don't use it.
  CmdLine(int argc, char* argv[]); // use this.
  ~CmdLine();

  // ******************************************************
  // arg is used to access the passed in strings with get,
  // argswitch is assumed to have a - before it.
  // required says whether or not it is required
  // nargs is the number of arguments after the switch.
  // 0 for unlimited: use all arguments until a switch is
  // encountered.
  // ******************************************************
  void addArgument(string arg, char argswitch,
		   bool required=false, int nargs=0);


  // ******************************************************
  // gets the arguments one by one, looping around when it 
  // reaches the last of them.
  // ******************************************************
  string getArgument(string arg);
  int getNumArgs(string arg);

 private:
  enum CmdError {REQUIRED_NOT_FOUND, TOO_MANY_ARGS,
		 TOO_FEW_ARGS, BAD_DEFAULT};

  class CmdLineArg
  {
  public:
    CmdLineArg(string na, bool req, int num);
    ~CmdLineArg();

    string name;
    vector<string> args;
    int nargs;
    int accessed;
    bool required;
  };

  vector<string> input;
  vector<CmdLineArg> args;

  // ******************************************************
  // used in addArgument. if a REQUIRED argument is not
  // found, will exit with EXIT_FAILURE.
  // ******************************************************
  void reportError(CmdError err);

  // ---
  CmdLineArg* find(string arg);


};


#endif
