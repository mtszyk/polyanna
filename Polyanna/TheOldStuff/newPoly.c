#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAXSIZE 64
#define MAXNAMESIZE 32
#define MAXHOUSESIZE 64
#define FILENAMESIZE 20
#define MAXFAMSIZE 100

/***************************************************************
 ***************************************************************
 ** The purpose of this program is to create a polyanna list  **
 ** that selects people for others to get presents for, and   **
 ** output that list to a file.                               **
 ** Matt Simmons, TA Rich Burns, Section 18, Project 3        **
 ***************************************************************
 ***************************************************************/

typedef struct {
  char name[MAXNAMESIZE];
  char household[MAXHOUSESIZE];
  char lastPoly[MAXNAMESIZE];
  char *assignment;
  char *assignHousehold;
} Person;

typedef enum { FALSE, TRUE } Bool;


/* --------------------- PROTOTYPES --------------------- */

void shuffleProcess(Person *family[], int size, int seed, Bool firstRun);
void shuffle(Person *family[], int size);
Bool collisionTest(Person *family[], int size);
void addPerson(Person *family[], int *size);
void removePerson2(Person *family[], int *size);
void removeProcess(Person *family[], int index, int size);

void writeFile(Person *family[], char fileName[], 
	       int famSize, Bool shuffled);

Person *readData(FILE *inputFile, int fileSize);
FILE *getFile(char fileName[]);
int menu();

void printData(Person *family[], int size);

/* ----------------------- MAIN ------------------------- */

int main(){

  FILE *inputFile;
  Person *family[MAXFAMSIZE];

  char fileName[FILENAMESIZE];
  Bool shuffled = FALSE;

  int fileSize = 0;
  int menuChoice;
  int *famSize = (int *)malloc(sizeof(int));
  int seed =  time(NULL); /* for use in shuffle function,
			   * which is recursive so defined here.
			   */

  inputFile = getFile(fileName);

  while ( !feof(inputFile) ){
    family[fileSize] = readData(inputFile, fileSize);
    fileSize++;
  }

  fclose(inputFile);

  *famSize = fileSize;

  do{

    menuChoice = menu();

    switch(menuChoice){
    case 1:
      addPerson(family, famSize);
      break;
    case 2:
      removePerson2(family, famSize);
      break;
    case 3:
      shuffleProcess(family, *famSize, seed, TRUE);
      printf("Shuffle complete.\n");
      shuffled = TRUE;
      break;
    case 4:
      printData(family, *famSize);
      break;
    case 5:
      exit(0);
      break;
    }

    if(shuffled != TRUE){
      printf("Another menu selection (if not, will save)? ");
      fflush(stdin);
      menuChoice = toupper(getchar()); // takes the first char
    }          /* and assigns the ascii
		* value to menuChoice.
		*/
    

  }while(menuChoice == 'Y' && shuffled != TRUE); 
  /* anything beginning with a y
   * (lower or upper) will continue
   * the loop. Same technique is 
   * used in getFile(), and ends
   * if shuffled.
   */

  writeFile(family, fileName, *famSize, shuffled);
  printf("Process complete, terminating.\n");

  return 0;

}

/* --------------------- FUNCTIONS ---------------------- */


/**********************************************************
 * The next three functions are my way to code this prog  *
 * to shuffle the data and get around the collisions.     *
 * The first function calls the shuffle with the second   *
 * function then tests for collisions in the third funct. *
 * If there are any, it calls itself with a new seed for  *
 * the rand function.                                     *
 **********************************************************/


void shuffleProcess(Person *family[], int famSize, int seed, Bool firstRun){

  int i;

  if(firstRun == TRUE)
    for(i = 0; i < famSize; i++)
      family[i]->assignment = family[i]->name; /* this is 
						* necessary because without
						* it, if someone was removed
						* from the list who was there
						* last year, then he'll be
						* there again. But I need to
						* have it set originally to 
						* lastPoly because if shuffle
						* isn't called, then lastPoly
						* data is lost.
						*/ 

  srand(seed);
  shuffle(family, famSize);

  if(collisionTest(family, famSize) == TRUE)
    shuffleProcess(family, famSize, seed+1, FALSE);

  return;

}

void shuffle(Person *family[], int size){

  char *swap;
  int shuffleNum;

  if( size < 2)
    return;

  size--;

  shuffleNum = rand() % size;

  swap = family[size]->assignment;
  family[size]->assignment = family[shuffleNum]->assignment;
  family[shuffleNum]->assignment = swap;

  swap = family[size]->assignHousehold;
  family[size]->assignHousehold = family[shuffleNum]->assignHousehold;
  family[shuffleNum]->assignHousehold = swap;

  shuffle(family, size);

  return;
}

Bool collisionTest(Person *family[], int size){

  size--;
  if (size < 0)
    return FALSE;

  if ( !strcmp(family[size]->household, family[size]->assignHousehold) ||
       !(strcmp(family[size]->assignment, family[size]->lastPoly)) )
    return TRUE;

  return collisionTest(family, size);
}


/**********************************************************
 * This function adds a person to the list, a max of 50   *
 * for this program.                                      *
 **********************************************************/

void addPerson(Person *family[], int *size){

  char temp[MAXSIZE];

  if ( *size >= MAXFAMSIZE ){
    printf("Your file is at it's max for this program.\n");
    return;
  }

  family[*size] = (Person *)(malloc(sizeof(Person)));

  printf("Input name: ");
  fflush(stdin);
  fgets(temp, MAXNAMESIZE, stdin);
  strtok(temp, "\n");
  strcpy(family[*size]->name, temp);
  strcpy(family[*size]->lastPoly, family[*size]->name);
  family[*size]->assignment = family[*size]->name; /* this sets
						    * the assignment
						    * pointer to it's
						    * own name like
						    * they all are
						    * to begin.
						    * (and last poly)
						    */


  printf("Input Household: ");
  fflush(stdin);
  fgets(temp, MAXHOUSESIZE, stdin);
  strtok(temp, "\n");
  strcpy(family[*size]->household, temp);
  family[*size]->assignHousehold = family[*size]->household; 

  (*size)++;

  return;
}


/**********************************************************
 * This function selects the person to remove from the    *
 * list, then the next function removes it.               *
 **********************************************************/

void removePerson(Person *family[], int *famSize){

  int count;
  int selection;

  char temp[5];

  for( count = 0; count < *famSize; count++ )
    printf("%d - %s\n", (count+1), family[count]->name);

  do{
    printf("Remove which number (-1 for no one): ");

    fflush(stdin);
    fgets(temp, 5, stdin);
    strtok(temp, "\n");

    selection = atoi(temp);

    if(selection == -1)
      return;
    else if(selection > *famSize || selection < 1)
      printf("Invalid selection.\n");

  }while(selection > *famSize || selection < 1);

  selection--;

  free(family[selection]);
  removeProcess(family, selection, *famSize);

  (*famSize)--;

  return;
}

void removeProcess(Person *family[], int index, int size){

  if(index == (size - 1)){
    free(family[size]);
    return;
  }

  family[index] = family[++index];

  removeProcess(family, index, size);

  return;
}


/**********************************************************
 * This function writes the data from the program to file *
 * Depending on whether or not the shuffle function was   *
 * executed or not, it will write the assignment file.    *
 **********************************************************/

void writeFile(Person *family[], char inputFile[],
	       int famSize, Bool shuffled){

  int count;
  FILE *output;
  char *outputName;

  switch(shuffled){
  case TRUE:
    printf("Writing assignments to files...\n");

    for( count = 0; count < famSize; count++ ){
      outputName = (char *)malloc((strlen(family[count]->name) + 11) 
				  * sizeof(char)); /* a bit messy.. 
						    * makes enough room for the string
						    * + Output/, .txt, and  a null char.
						    */
      strcpy(outputName, "Output/");
      strcat(outputName, family[count]->name);
      strcat(outputName, ".txt");

      output = fopen(outputName, "w");
      
      fprintf(output, "Greetings %s, of the %s.\nYou have been invited to the"
	            " VFW in National Park this year for\nPollyanna on Saturday,"
	            " December 27. (Exact time TBD) Your Pollyanna\nthis year is"
	            " %s of the %s.\n\nSuggested Expenditures:\n"
	            "    Little Kids : $15 - $20\n"
	            "    Older Kids : $25 - $30\n"
	            "    Really Old Kids : $50 - $60\n\n"
	            "Come ready to enjoy music, food, and family!\n\n"
	            "We don't look into each person's personal invitation!\n"
	            "If there are "
	            "any problems with the info you\nrecieved, please call "
	            "Mike Frizell at 703-577-1129 IMMEDIATELY!\n\n"
	      "Merry Christmas, happy Pollyanna!",
	      family[count]->name, family[count]->household,
	      family[count]->assignment, family[count]->assignHousehold);

      fclose(output);
      free(outputName);

    }


  case FALSE:
    printf("Writing changes to data file: %s\n", inputFile);
    output = fopen(inputFile, "w");
    
    for( count = 0; count < famSize; count++ ){
      fprintf(output, "%s\n", family[count]->name);
      fprintf(output, "%s\n", family[count]->household);
      fprintf(output, "%s", family[count]->assignment);
      if(count != famSize - 1)
	fprintf(output, "\n");
    }

    fclose(output);

    break; 
  } 
  
  return;
}

/**********************************************************
 * The purpose of this function is to simply get the data *
 * out of the datafile, and prepare it for usage in the   *
 * program... and I figured that it would just be more    *
 * confusing if I put the fgets etc in another function   *
 * since I only call it three times. if this was main, it *
 * would be in it's own function.                         *
 **********************************************************/

Person *readData(FILE *inputFile, int fileSize){

  int count;

  Person *family = (Person *)malloc(sizeof(Person));

  char delims[] = {'\n', 13, '\0'}; /* I have no idea why but
				     * in all programs I've written
				     * that reads data from a file,
				     * the program LOVES to add character
				     * 13 to the end of the string.
				     * Character 13 is the character that
				     * says "go back to the start of
				     * THIS line" so I get some nasty
				     * behaviors if I don't have this
				     */
  char temp[MAXSIZE];

  fgets(temp, MAXNAMESIZE, inputFile);
  strtok(temp, delims);
  strcpy(family->name, temp);

  fgets(temp, MAXHOUSESIZE, inputFile);
  strtok(temp, delims);
  strcpy(family->household, temp);
  family->assignHousehold = family->household;

  fgets(temp, MAXNAMESIZE, inputFile);
  strtok(temp, delims);
  strcpy(family->lastPoly, temp);
  family->assignment = family->lastPoly;

  return family;
}


/**********************************************************
 * This function gets the name of the file that the user  *
 * wants to run the program with.                         *
 **********************************************************/


FILE *getFile(char fileName[]){

  FILE *inputFile;

  Bool newFile = TRUE;
  Bool runProcess = FALSE;

  char contTest;

  do{

    printf("Enter dataFile: ");
    fflush(stdin);
    fgets(fileName, FILENAMESIZE, stdin);
    strtok(fileName, "\n");
    
    inputFile = fopen(fileName, "r");

    if ( inputFile == NULL ){

      printf("Invalid file, enter new filename? ");
      fflush(stdin);
      contTest = toupper(getchar()); 

      if(contTest != 'Y')
	newFile = FALSE;
      
    }
    else{
      runProcess = TRUE;
    }


  }while(newFile == TRUE && runProcess == FALSE);

  if(runProcess == FALSE)
    exit(1);

  printf("File opened successfully.\n");
  return inputFile;
}

/**********************************************************
 * The most complex function ever writen... it's a menu!  *
 **********************************************************/

int menu(){

  int choice = -1;
  char input[5];

  printf("***********************************\n"
	  "* Please select Menu Choice       *\n"
	  "*                                 *\n"
	  "* 1: Add person to the list       *\n"
	  "* 2: Remove person from the list  *\n"
	  "* 3: Run polyanna shuffle         *\n"
	  "* 4: Print Data                   *\n"
	  "* 5: Exit without saving          *\n"
	  "*                                 *\n"
	  "***********************************\n\n"
	 "Choice: ");
  do{

    if(choice != -1)
      printf("Selection invalid.\nChoice: ");
    
    fflush(stdin);
    fgets(input, 5, stdin);
    choice = atoi(input);
    
  }while(choice < 1 || choice > 5);

  return choice;
}


/**********************************************************
 * Prints the data in the array of pointers/structs       *
 **********************************************************/


void printData(Person *family[], int size){

  int count;

  for( count = 0; count < size; count++ ){
    printf("%s in %s\n", family[count]->name, family[count]->household);
    printf("had %s last year.\n", family[count]->lastPoly);
  }

  return;
}








void removePerson2(Person *family[], int *famSize){

  free(family[3]);
  removeProcess(family, 3, *famSize);

  (*famSize)--;

  return;
}
