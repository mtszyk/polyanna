Greetings Dena Kirk, of the The House of Dennis and Annemarie Burke.
You have been invited to the VFW in National Park this year for
Pollyanna on Saturday, December 27. (Exact time TBD) Your Pollyanna
this year is Emily Shimp of the The House of Dave and Jennifer Shimp.

Suggested Expenditures:
    Little Kids : $15 - $20
    Older Kids : $25 - $30
    Really Old Kids : $50 - $60

Come ready to enjoy music, food, and family!

We don't look into each person's personal invitation!
If there are any problems with the info you
recieved, please call Mike Frizell at 703-577-1129 IMMEDIATELY!

Merry Christmas, happy Pollyanna!