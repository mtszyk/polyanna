/* pollyanna.h Matt Simmons
 * pollyanna people for the frizell family pollyanna!
 */

#ifndef POLLY_H
#define POLLY_H

#include <iostream>

class Pollyanna_C
{
 public:

  Pollyanna_C(); // default constructor
  Pollyanna_C(const Pollyanna_C &polly); // copy constructor
  Pollyanna_C(string theName, string theLastPolly,
	      string theAddress, string theHousehold,
	      int theAgeGroup); // input constructor
  // assignment operator overload
  Pollyanna_C & operator = (const Pollyanna_C &rvalue);

  // mutators
  void setName(string input) {name = input;}
  void setLastPolly(string input) { lastPolly = input;}
  void setAddress(string input) { address = input;}
  void setHousehold(string input) { household = input;}
  void setNewPolly(string *input) { newPolly = input;}
  void setAgeGroup(int input) { ageGroup= input;}

  // accessors
  string getName() const {return name;}
  string getLastPolly() const {return lastPolly;}
  string getAddress() const {return address;}
  string getHousehold() const {return household;}
  int getAgeGroup() const {return ageGroup;}

  // other
  void print(std::ostream &out) const;

 private:
  string name;
  string lastPolly;
  string address;
  string household;
  string *newPolly;
  int ageGroup;
};

std::ostream & operator << (std::ostream & left, const Pollyanna_C & right);

#endif
