Greetings Mike Cucuglielo, of the The House of Mike and K'leen Cucugliello.
You have been invited to the VFW in National Park this year for
Pollyanna on Saturday, December 27. (Exact time TBD) Your Pollyanna
this year is Gary Simmons of the The House of Gary and Regina Simmons .

Suggested Expenditures:
    Little Kids : $15 - $20
    Older Kids : $25 - $30
    Really Old Kids : $50 - $60

Come ready to enjoy music, food, and family!

We don't look into each person's personal invitation!
If there are any problems with the info you
recieved, please call Mike Frizell at 703-577-1129 IMMEDIATELY!

Merry Christmas, happy Pollyanna!