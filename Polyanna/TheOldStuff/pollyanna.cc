/* pollyanna.cc Matt Simmons section 021 cisc 181
 * definitons for pollyanna.h
 */

#include <iostream>
using std::string;
using std::endl;

#include "pollyanna.h"
/*******************************************************
 * default constructor, strings are automatically null *
 * to begin with from their default constructor, so    *
 * they need not be initialized                        *
 *******************************************************/
Pollyanna_C::Pollyanna_C()
{
  newPolly = &name;
  ageGroup = 0;
}

/*******************************************************
 * copy constructor                                    *
 *******************************************************/
Pollyanna_C::Pollyanna_C(const Pollyanna_C &original)
{
  name = original.name;
  lastPolly = original.lastPolly;
  address = original.address;
  household = original.household;
  ageGroup = original.ageGroup;
  newPolly = original.newPolly;
}

/*******************************************************
 * constructor with all the information                *
 *******************************************************/
Pollyanna_C::Pollyanna_C(string theName, string theLastPolly,
			 string theAddress, string theHousehold,
			 int theAgeGroup)
{
  name = theName;
  lastPolly = theLastPolly;
  address = theAddress;
  household = theHousehold;
  ageGroup = theAgeGroup;
  newPolly = &name;
}


/*******************************************************
 * overloaded assignment operator                      *
 *******************************************************/
Pollyanna_C & Pollyanna_C::operator = (const Pollyanna_C &rvalue)
{

  if(this == &rvalue) // like x=x or q=q
    return *this;

  name = rvalue.name;
  lastPolly = rvalue.lastPolly;
  address = rvalue.address;
  household = rvalue.household;
  ageGroup = rvalue.ageGroup;
  newPolly = rvalue.newPolly;

  return (*this);
}

/*******************************************************
 * overloaded ostream insertion operator               *
 *******************************************************/
std::ostream & operator << (std::ostream & left, const Pollyanna_C & right)
{
  right.print(left);
  return left;

}

/*******************************************************
 * prints out the values of the class                  *
 *******************************************************/
void Pollyanna_C::print(std::ostream &out) const
{
  out << "Name: " << name << endl
      << "Last Pollyanna: " << lastPolly << endl
      << "Address: " << address << endl
      << "Household: " << household << endl
      << "ageGroup: " << ageGroup << endl;

}
