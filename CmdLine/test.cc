#include <iostream>

#include "CmdLine.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{

  CmdLine cmd(argc, argv);

  cmd.addArgument("default", '-', true, 1);
  cout << "moo" << endl;
  cmd.addArgument("first", 'f', false, 0);
  cmd.addArgument("second", 's', true, 2);

  for(int i = 0; i < cmd.getNumArgs("default"); i++)
    cout << cmd.getArgument("default") << endl;
  
  for(int i = 0; i < cmd.getNumArgs("first"); i++)
    cout << cmd.getArgument("first") << endl;
  
  cout << cmd.getArgument("second") << endl;
  cout << cmd.getArgument("second") << endl;


  return 0;
}
